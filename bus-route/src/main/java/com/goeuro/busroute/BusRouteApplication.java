package com.goeuro.busroute;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BusRouteApplication {
    public static void main(String[] args) throws Exception {
        SpringApplication.run(BusRouteApplication.class, args);
    }
}
