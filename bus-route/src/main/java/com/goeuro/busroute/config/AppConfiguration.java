package com.goeuro.busroute.config;

import com.goeuro.busroute.controllers.AllRequestsInterceptor;
import com.goeuro.busroute.service.RoutesService;
import org.springframework.boot.ApplicationArguments;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.io.File;

import static org.apache.commons.lang3.ArrayUtils.isEmpty;

@Configuration
@PropertySource("classpath:route.properties")
public class AppConfiguration extends WebMvcConfigurerAdapter {

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyConfigIn() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Override
    public void addInterceptors(final InterceptorRegistry registry) {
        registry.addInterceptor(new AllRequestsInterceptor());
    }

    @Bean
    public RoutesService routesService(ApplicationArguments applicationArguments) {
        String[] sourceArgs = applicationArguments.getSourceArgs();
        if (isEmpty(sourceArgs)){
            throw new RuntimeException("Path to data file is not specified");
        }
        String pathname = sourceArgs[0];
        return new RoutesService(new File(pathname));
    }
}