package com.goeuro.busroute.controllers;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.goeuro.busroute.utils.RouteConstants.REQUEST_HANDLER_ATTRIBUTE_NAME;

public class AllRequestsInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(final HttpServletRequest request, final HttpServletResponse response, final Object handler) {
        request.setAttribute(REQUEST_HANDLER_ATTRIBUTE_NAME, handler);
        return true;
    }

}
