package com.goeuro.busroute.controllers;

import com.goeuro.busroute.dto.DirectDTO;
import com.goeuro.busroute.service.RoutesService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

import static com.goeuro.busroute.utils.RouteConstants.API_DIRECT;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
public class BusRouteController {

    @Resource
    private RoutesService routesService;

    @RequestMapping(path = API_DIRECT, produces = APPLICATION_JSON_UTF8_VALUE, method = GET)
    public DirectDTO direct(@RequestParam(name = "dep_sid") int from, @RequestParam(name = "arr_sid") int to) {
        boolean routeExists = routesService.existDirectRoute(from, to);
        return DirectDTO.builder().depSid(from).arrSid(to).directBusRoute(routeExists).build();
    }
}
