package com.goeuro.busroute.controllers;

import com.goeuro.busroute.dto.ErrorDTO;
import org.slf4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;

import static com.goeuro.busroute.utils.RouteConstants.REQUEST_HANDLER_ATTRIBUTE_NAME;
import static org.slf4j.LoggerFactory.getLogger;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.util.StringUtils.isEmpty;
import static org.springframework.web.servlet.HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE;

@ControllerAdvice
public class GlobalControllersAdvise extends ResponseEntityExceptionHandler {

    private static final Logger logger = getLogger(GlobalControllersAdvise.class);

    @ExceptionHandler({Exception.class})
    @ResponseStatus(INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ErrorDTO handleException(final Exception exception, final HttpServletRequest request) {
        logger.error("Critical application error. Request handler: {}", getRequestHandlerAsString(request), exception);
        return buildFromException(exception);
    }

    private ErrorDTO buildFromException(Exception ex) {
        return ErrorDTO.builder().message(ex.getMessage()).build();
    }

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body,
                                                             HttpHeaders headers, HttpStatus status, WebRequest request) {
        headers.setContentType(APPLICATION_JSON_UTF8);
        notifyAboutHandler(ex, request);
        return super.handleExceptionInternal(ex, buildFromException(ex), headers, status, request);
    }

    private String getRequestHandlerAsString(final HttpServletRequest request) {
        final Object requestHandler = request.getAttribute(REQUEST_HANDLER_ATTRIBUTE_NAME);
        if (requestHandler != null) {
            return requestHandler.toString();
        }
        String attribute = (String) request.getAttribute(PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
        return !isEmpty(attribute) ? attribute : "No handler";
    }

    private void notifyAboutHandler(Exception ex, WebRequest request) {
        String requestHandler = null;
        if (request instanceof NativeWebRequest) {
            NativeWebRequest nativeWebRequest = (NativeWebRequest) request;
            requestHandler = getRequestHandlerAsString(nativeWebRequest.getNativeRequest(HttpServletRequest.class));
        }
        logger.error("Native spring exception. Request handler: {}", requestHandler, ex);
    }

}
