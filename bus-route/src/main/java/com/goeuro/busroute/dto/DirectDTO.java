package com.goeuro.busroute.dto;

import lombok.Builder;
import lombok.Getter;


@Builder
@Getter
public class DirectDTO {
    private Integer depSid;

    private Integer arrSid;

    private boolean directBusRoute;
}
