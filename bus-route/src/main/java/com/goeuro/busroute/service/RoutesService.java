package com.goeuro.busroute.service;

import com.google.common.base.Splitter;
import com.google.common.io.LineProcessor;
import org.apache.lucene.util.OpenBitSet;
import org.slf4j.Logger;
import org.springframework.beans.factory.InitializingBean;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static com.goeuro.busroute.utils.RouteConstants.MAX_ROUTES_COUNT;
import static com.goeuro.busroute.utils.RouteConstants.MAX_STATION_COUNT;
import static com.google.common.base.Charsets.UTF_8;
import static com.google.common.io.Files.readLines;
import static java.lang.Integer.parseInt;
import static org.slf4j.LoggerFactory.getLogger;
import static org.springframework.util.CollectionUtils.isEmpty;

public class RoutesService implements InitializingBean {

    private static final Logger logger = getLogger(RoutesService.class);

    private static final Splitter splitter = Splitter.on(" ").omitEmptyStrings();

    private final File fileWithRoutes;

    private OpenBitSet[] stationsWithRoutes = new OpenBitSet[MAX_STATION_COUNT];

    public RoutesService(File fileWithRoutes) {
        this.fileWithRoutes = fileWithRoutes;
    }


    public boolean existDirectRoute(int from, int to) {
        OpenBitSet routesForFirstStation = stationsWithRoutes[from];
        if (routesForFirstStation == null) {
            return false;
        }
        if (from == to) {
            return true;
        }
        OpenBitSet routesForSecondStation = stationsWithRoutes[to];
        return routesForSecondStation != null && routesForFirstStation.intersects(routesForSecondStation);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        logger.info("Start load routes from source file");
        if (!fileWithRoutes.exists()) {
            logger.warn("Source file does not exist");
            throw new RuntimeException("File with name " + fileWithRoutes.getAbsolutePath() + " not exist");
        }
        try {
            readLines(fileWithRoutes, UTF_8, new LineProcessor<Object>() {
                        @Override
                        public boolean processLine(String line) throws IOException {
                            List<String> strings = splitter.splitToList(line);
                            if (isEmpty(strings)) {
                                //TODO fixme. It is better to have line number
                                throw new RuntimeException("There is invalid line");
                            }
                            if (strings.size() > 1) {
                                final int routeId = parseInt(strings.get(0));
                                for (int i = 1, n = strings.size(); i < n; i++) {
                                    int stationNumberAsInt = parseInt(strings.get(i));
                                    OpenBitSet routesForStation = stationsWithRoutes[stationNumberAsInt];
                                    if (routesForStation == null) {
                                        routesForStation = new OpenBitSet(MAX_ROUTES_COUNT);
                                        stationsWithRoutes[stationNumberAsInt] = routesForStation;
                                    }
                                    routesForStation.set(routeId);
                                }
                            }
                            return true;
                        }

                        @Override
                        public Object getResult() {
                            return null;
                        }
                    }
            );
            logger.info("Source file loaded successfully");
        } catch (IOException e) {
            logger.error("Error during load from source file", e);
            throw new RuntimeException(e);
        }
    }
}
