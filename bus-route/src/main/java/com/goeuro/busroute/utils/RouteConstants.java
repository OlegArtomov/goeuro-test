package com.goeuro.busroute.utils;

public final class RouteConstants {

    private RouteConstants(){

    }

    public static final String API_DIRECT = "/api/direct";

    public static final String REQUEST_HANDLER_ATTRIBUTE_NAME = "REQUEST_HANDLER_ATTRIBUTE_NAME";

    public static final int MAX_STATION_COUNT = 1000000;

    public static final int MAX_ROUTES_COUNT = 100000;
}
