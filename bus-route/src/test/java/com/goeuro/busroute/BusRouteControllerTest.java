package com.goeuro.busroute;


import com.goeuro.busroute.controllers.BusRouteController;
import com.goeuro.busroute.service.RoutesService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockBeans;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.annotation.Resource;

import static com.goeuro.busroute.utils.RouteConstants.API_DIRECT;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = {BusRouteController.class})
@MockBeans(@MockBean(RoutesService.class))
public class BusRouteControllerTest {

    @Resource
    private MockMvc mockMvc;

    @Resource
    private RoutesService routesService;

    @Test
    public void testResponseWhenRouteExists() throws Exception {
        int from = 1;
        int to = 2;
        when(routesService.existDirectRoute(eq(from), eq(to))).thenReturn(true);
        mockMvc.perform(get(API_DIRECT)
                .param("dep_sid", String.valueOf(from))
                .param("arr_sid", String.valueOf(to)))
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.dep_sid").value(from))
                .andExpect(jsonPath("$.arr_sid").value(to))
                .andExpect(jsonPath("$.direct_bus_route").value(true)
                );
    }


    @Test
    public void testResponseWhenRouteIsMissing() throws Exception {
        int from = 1;
        int to = 2;
        when(routesService.existDirectRoute(eq(from), eq(to))).thenReturn(false);
        mockMvc.perform(get(API_DIRECT)
                .param("dep_sid", String.valueOf(from))
                .param("arr_sid", String.valueOf(to)))
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.dep_sid").value(from))
                .andExpect(jsonPath("$.arr_sid").value(to))
                .andExpect(jsonPath("$.direct_bus_route").value(false)
                );
    }

    @Test
    public void testResponseWhenNoRequiredParameterWithStation() throws Exception {
        int from = 1;
        mockMvc.perform(get(API_DIRECT)
                .param("dep_sid", String.valueOf(from)))
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("Required int parameter 'arr_sid' is not present")
                );
    }

}